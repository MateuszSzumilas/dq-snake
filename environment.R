env_new_prize <- function (env)
{
  repeat
  {
    prize = list(x = ceiling(runif(1)*env$size),
                 y = ceiling(runif(1)*env$size))
    
    on_snake <- sapply(env$snake, function(X) { identical(prize, X) })
    
    if (sum(on_snake) == 0) break;
  }
  
  return(prize)
}

env_reset <- function(size) 
{
  snake = list(list(x = ceiling(runif(1)*size),
                    y = ceiling(runif(1)*size)))
    
  env <- list(size = size,
              actions = c("L", "A", "R"),
              dir = ceiling((runif(1)*100) %% 4),
              snake = snake,
              length = length(snake),
              end = F,
              reward = 0)
  
  env$prize <- env_new_prize(env)
  
  return(env)
}

env_action <- function(env, action)
{
  if (env$end) {
    env$reward = 0
    return(env)
  }
  
  head_pos <- list(x = env$snake[[env$length]]$x,
                   y = env$snake[[env$length]]$y)
  
  if (action == "L")
  {
    env$dir <- env$dir - 1
    if (env$dir < 1) env$dir = 4
  }
  else if (action == "R")
  {
    env$dir <- env$dir + 1
    if (env$dir > 4) env$dir = 1
  }
  
  snake_dirs <- c("L", "U", "R", "D")
  dir_tmp = snake_dirs[env$dir]
  
  if (dir_tmp == "L")
    head_pos$x = head_pos$x-1
  else if (dir_tmp == "R")
    head_pos$x = head_pos$x+1
  else if (dir_tmp == "U")
    head_pos$y = head_pos$y+1
  else if (dir_tmp == "D")
    head_pos$y = head_pos$y-1

  tmp_end = F
  
  #check prize
  if(identical(env$prize, head_pos))
  {
    tmp_reward = 1
    
    env$length <- env$length+1
    env$snake[[env$length]] <- head_pos
    
    env$prize = env_new_prize(env)
  }
  else
  {
    tmp_reward = 0
    
    env$snake[[1]] <- NULL
    env$snake[[env$length]] <- head_pos
  }
  
  #check border
  if ( head_pos$x == 0 ||
       head_pos$y == 0 ||
       head_pos$x == env$size+1 ||
       head_pos$y == env$size+1 )
  {
    tmp_end = T
    
    tmp_reward = -1
  }
  
  #check collision
  if (env$length > 1)
    if ( sum(sapply(env$snake[-env$length], function(X) { identical(head_pos, X) })) > 0 )
    {
      tmp_end = T
      
      tmp_reward = -1
    }
  
  env$reward = tmp_reward
  env$end = tmp_end
  
  return(env)
}

env_plot <- function(env, dev_off=F)
{
  old_par <- par()
  par(pty = "s")
  
  if(dev_off) dev.off()
  
  plot(0,0, type="n",
       xlim = c(0,env$size+1),
       ylim = c(0,env$size+1),
       axes = F,
       xlab="", ylab="")
  
  abline(v = 0)
  abline(h = 0)
  abline(v = env$size+1)
  abline(h = env$size+1)
  
  snake <- data.frame(do.call(rbind, env$snake))
  
  points(snake$x[env$length], snake$y[env$length], col="darkblue", pch=16, cex=1.6)
  if (env$length > 1)
    points(snake$x[-env$length], snake$y[-env$length], col="darkcyan", pch=16)
  
  points(env$prize$x, env$prize$y, col="red", pch=15)
  
  title(env$reward)
  par(pty = old_par$pty)
}

env_map <- function(env)
{
  map <- matrix(0, env$size + 2, env$size + 2)
  map[1,] <- "b"
  map[,1] <- "b"
  map[env$size + 2, ] <- "b"
  map[ ,env$size + 2] <- "b"
  map[env$prize$x + 1, env$prize$y + 1] <- "p"
  
  snake <- as.matrix(do.call(rbind.data.frame, env$snake))
  map[snake + 1] <- "s"
  
  map
}

env_scan <- function(head_x, head_y, map, dir_inc)
{
  head_x <- head_x + 1
  head_y <- head_y + 1
  dist <- 0
  
  repeat
  {
    head_x <- head_x + dir_inc[1]
    head_y <- head_y + dir_inc[2]
    dist <- dist + 1
    
    if (map[head_x, head_y] != "0")
      break;
  }
  
  return(list(res = map[head_x, head_y],
              dist = dist))
}

# global directions
global_dirs <- matrix(c(c(0,1),
                        c(1,1),
                        c(1,0),
                        c(1,-1),
                        c(0,-1),
                        c(-1,-1),
                        c(-1,0),
                        c(-1,1)),
                      byrow = T,
                      nrow = 8)
# rows are relative directions mappings of global_dirs from respective 
# snake directions: left/up/right/down
#
# each row consists of 'global_dirs' indices of relative directions
# in order: left, la, ahead, ra, right
relative_from_global_LURD <- matrix(c(c(5,6,7,8,1),
                                      c(7,8,1,2,3),
                                      c(1,2,3,4,5),
                                      c(3,4,5,6,7)),
                                    nrow = 4,
                                    byrow = T)

#' returns:
#' head to (by rows) border/prize/snake reciprocal of distances 
#' ordered as (by cols): left, la, ahead, ra, right
env_state <- function(env)
{
  if(env$end) 
    return(matrix(-1, nrow = 3, ncol = 5))
  
  h_x <- env$snake[[env$length]]$x
  h_y <- env$snake[[env$length]]$y
  map <- env_map(env)
  
  relative_dirs <- relative_from_global_LURD[env$dir, ]
  
  dir_left  <- env_scan(h_x, h_y, map, global_dirs[relative_dirs[1],])
  dir_la    <- env_scan(h_x, h_y, map, global_dirs[relative_dirs[2],])
  dir_ahead <- env_scan(h_x, h_y, map, global_dirs[relative_dirs[3],])
  dir_ra    <- env_scan(h_x, h_y, map, global_dirs[relative_dirs[4],])
  dir_right <- env_scan(h_x, h_y, map, global_dirs[relative_dirs[5],])
  
  scan_tot <- rbind.data.frame(dir_left, dir_la, dir_ahead, dir_ra, dir_right)
  scan_tot$res <- as.character(scan_tot$res)
  res_row_by_scan <- list(b = 1, p = 2, s = 3)
  
  res <- matrix(0, nrow = 3, ncol = 5)
  
  res[ res_row_by_scan[[ scan_tot$res[1] ]], 1] <- 1/scan_tot$dist[1]
  res[ res_row_by_scan[[ scan_tot$res[2] ]], 2] <- 1/scan_tot$dist[2]
  res[ res_row_by_scan[[ scan_tot$res[3] ]], 3] <- 1/scan_tot$dist[3]
  res[ res_row_by_scan[[ scan_tot$res[4] ]], 4] <- 1/scan_tot$dist[4]
  res[ res_row_by_scan[[ scan_tot$res[5] ]], 5] <- 1/scan_tot$dist[5]
  
  rownames(res) <- c("b", "p", "s")
  colnames(res) <- c("l","la","a","ra","r")
  
  res
}
